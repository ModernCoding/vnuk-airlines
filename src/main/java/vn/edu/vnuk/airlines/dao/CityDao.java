	package vn.edu.vnuk.airlines.dao;

import java.sql.Connection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.stereotype.Repository;

import vn.edu.vnuk.airlines.model.City;
import vn.edu.vnuk.airlines.model.Country;

@Repository
public class CityDao {

	private Connection connection;

	 public void setDataSource(DataSource dataSource) {
	        
	        try {
	 	    	this.connection = dataSource.getConnection();
	 	    } catch (SQLException e) {
	 	    	throw new RuntimeException(e);
	 	    }
	    }
	    public void create(City cities) throws SQLException{

	        String sqlQuery = "insert into cities (name, country_id)  "
	                        +	"values (?, ?)";

	        PreparedStatement statement;

	        try {
	                statement = connection.prepareStatement(sqlQuery);

	                //	Replacing "?" through values
	                statement.setString(1, cities.getName());
	                statement.setLong(2, cities.getCountryId());
	               

	                // 	Executing statement
	                statement.execute();

	                System.out.println("New record in DB !");

	        } catch (Exception e) {
	                // TODO Auto-generated catch block
	                e.printStackTrace();
	        } finally {
	                System.out.println("Done !");
	                
	        }

	    }
	    //  READ (List of Cities)
	    @SuppressWarnings("finally")
	    public List<City> read() throws SQLException {

	        String sqlQuery = "select cities.id as city_id, cities.name as city_name,  countries.name as country_name from cities , countries  where cities.country_id = countries.id";
	        PreparedStatement statement;
	        List<City> cities = new ArrayList<City>();

	        try {

	            statement = connection.prepareStatement(sqlQuery);

	            // 	Executing statement
	            ResultSet results = statement.executeQuery();
	            
	            while(results.next()){

	                City city = new City();
	                Country country = new Country();
	                
	                city.setId(results.getLong("city_id"));
	                city.setName(results.getString("city_name"));
	                city.setCountry(country);
	                
	                
	                country.setName(results.getString("country_name"));

	                
	                cities.add(city);

	            }

	            results.close();
	            statement.close();


	        } catch (Exception e) {
	                // TODO Auto-generated catch block
	                e.printStackTrace();
	        } finally {
	                
	                return cities;
	        }


	    }
	    //Read single 

	    
	    @SuppressWarnings({ "finally" })
		public City read(int id) throws SQLException{

	        String sqlQuery = "select cities.id as city_id, cities.name as city_name, countries.name as country_name from cities , countries where cities.id=? and cities.country_id = countries.id;";

	        PreparedStatement statement;
	        City city = new City();
	        Country country = new Country();
	        
	        try {
	            statement = connection.prepareStatement(sqlQuery);

	            //	Replacing "?" through values
	            statement.setLong(1, id);

	            // 	Executing statement
	            ResultSet results = statement.executeQuery();

	            if(results.next()){

	            	city.setId(results.getLong("city_id"));
	            	city.setName(results.getString("city_name"));
	            	city.setCountry(country);
	            	
	            	country.setName(results.getString("country_name"));

	              

	            statement.close();

	        } }catch (Exception e) {
	                // TODO Auto-generated catch block
	                e.printStackTrace();
	        } finally {

	            
	            return city;
	        }

	    
	    }
	    
	//  UPDATE
	    public void update(City cities) throws SQLException {
	        String sqlQuery = "update cities set name=?, country_id=? where id=?";
	        
	        try {
	            PreparedStatement statement = connection.prepareStatement(sqlQuery);
	            statement.setString(1, cities.getName());
	            statement.setLong(2, cities.getCountryId());
	            statement.setLong(3, cities.getId());
	            statement.execute();
	            statement.close();
	            
	            System.out.println("cities successfully modified.");
	        } 

	        catch (Exception e) {
	            // TODO Auto-generated catch block
	            e.printStackTrace();
	        }
	   }
	    
//  DELETE
    public void delete(int id) throws SQLException {
        String sqlQuery = "delete from cities where id=?";

        try {
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setLong(1, id);
            statement.execute();
            statement.close();
            
            System.out.println("City successfully deleted.");

        } 

        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    } 
	    
	    
}

package vn.edu.vnuk.airlines.dao;

import java.sql.Connection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.stereotype.Repository;

import vn.edu.vnuk.airlines.model.PlaneManufacture;
import vn.edu.vnuk.airlines.model.PlaneModel;

@Repository
public class PlaneModelDao {
	
	private Connection connection;

	 public void setDataSource(DataSource dataSource) {
	        
	        try {
	 	    	this.connection = dataSource.getConnection();
	 	    } catch (SQLException e) {
	 	    	throw new RuntimeException(e);
	 	    }
	    }
	 // CREATE
	    public void create(PlaneModel planemodels) throws SQLException{

	        String sqlQuery = "insert into plane_models (name, plane_manufacture_id)  "
	                        +	"values (?, ?)";

	        PreparedStatement statement;

	        try {
	                statement = connection.prepareStatement(sqlQuery);

	                //	Replacing "?" through values
	                statement.setString(1, planemodels.getName());
	                statement.setLong(2, planemodels.getPlaneManufactureId());
	               

	                // 	Executing statement
	                statement.execute();

	                System.out.println("New record in DB !");

	        } catch (Exception e) {
	                // TODO Auto-generated catch block
	                e.printStackTrace();
	        } finally {
	                System.out.println("Done !");
	        }

	    }
	    //  READ (List of PlaneModels)
	    @SuppressWarnings("finally")
	    public List<PlaneModel> read() throws SQLException {

	        String sqlQuery = "select t1.id as planeModel_id, t1.name as planeModel_name, t2.name as planeManufacture_name from plane_models t1, plane_manufactures t2 where t1.plane_manufacture_id = t2.id";
	        PreparedStatement statement;
	        List<PlaneModel> planemodels = new ArrayList<PlaneModel>();

	        try {

	            statement = connection.prepareStatement(sqlQuery);

	            // 	Executing statement
	            ResultSet results = statement.executeQuery();
	            
	            while(results.next()){

	                PlaneModel planeModel = new PlaneModel();
	                PlaneManufacture planeManufacture = new PlaneManufacture();
	                
	                planeModel.setId(results.getLong("planeModel_id"));
	                planeModel.setName(results.getString("planeModel_name"));
	                planeModel.setPlaneManufacture(planeManufacture);
	                
	         
	                planeManufacture.setName(results.getString("planeManufacture_name"));
	                
	                
	                planemodels.add(planeModel);

	            }

	            results.close();
	            statement.close();


	        } catch (Exception e) {
	                // TODO Auto-generated catch block
	                e.printStackTrace();
	        } finally {
	               
	                return planemodels;
	        }


	    }
	    //Read single 

	    
	    @SuppressWarnings({ "finally" })
		public PlaneModel read(int id) throws SQLException{

	        String sqlQuery = "select t1.id as planeModel_id, t1.name as planeModel_name, t2.name as planeManufacture_name from plane_models t1, plane_manufactures t2 where t1.id=? and t1.plane_manufacture_id = t2.id";

	        PreparedStatement statement;
	        PlaneModel planeModel = new PlaneModel();
	        PlaneManufacture planeManufacture = new PlaneManufacture();
	        
	        try {
	            statement = connection.prepareStatement(sqlQuery);

	            //	Replacing "?" through values
	            statement.setLong(1, id);

	            // 	Executing statement
	            ResultSet results = statement.executeQuery();

	            if(results.next()){

	            	 planeModel.setId(results.getLong("planeModel_id"));
		                planeModel.setName(results.getString("planeModel_name"));
		                planeModel.setPlaneManufacture(planeManufacture);
		                
		         
		                planeManufacture.setName(results.getString("planeManufacture_name"));

	              

	            statement.close();

	        } }catch (Exception e) {
	                // TODO Auto-generated catch block
	                e.printStackTrace();
	        } finally {
	            

	            
	            return planeModel;
	        }

	    
	    }
	    
	//  UPDATE
	    public void update(PlaneModel planeModel) throws SQLException {
	        String sqlQuery = "update plane_models set name=?, plane_manufacture_id=?" 
	        		 		+" where id=?";
	        
	        try {
	            PreparedStatement statement = connection.prepareStatement(sqlQuery);
	            statement.setString(1, planeModel.getName());
	            statement.setLong(2, planeModel.getPlaneManufactureId());
	            statement.setLong(3, planeModel.getId());
	            statement.execute();
	            statement.close();
	            
	            System.out.println("Plane_models successfully modified.");
	        } 

	        catch (Exception e) {
	            // TODO Auto-generated catch block
	            e.printStackTrace();
	        }
	        
	        finally {
	            
	        }
	        
	    }
	    
	//  DELETE
	    public void delete(int id) throws SQLException {
	        String sqlQuery = "delete from plane_models where id=?";

	        try {
	            PreparedStatement statement = connection.prepareStatement(sqlQuery);
	            statement.setLong(1, id);
	            statement.execute();
	            statement.close();
	            
	            System.out.println("Plane_models successfully deleted.");

	        } 

	        catch (Exception e) {
	            // TODO Auto-generated catch block
	            e.printStackTrace();
	        }

	    }
	    
	    
	    
}

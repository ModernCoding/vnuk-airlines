package vn.edu.vnuk.airlines.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.stereotype.Repository;

import vn.edu.vnuk.airlines.model.Airport;
import vn.edu.vnuk.airlines.model.Route;
import vn.edu.vnuk.airlines.model.Ticket;



@Repository
public class TicketDao {

	private Connection connection;

	 public void setDataSource(DataSource dataSource) {
	        
	        try {
	 	    	this.connection = dataSource.getConnection();
	 	    } catch (SQLException e) {
	 	    	throw new RuntimeException(e);
	 	    }
   }
	 
	//  READ (List of Cities)
	    @SuppressWarnings("finally")
	    public List<Ticket> read() throws SQLException {

	        String sqlQuery = "select a_fr.code from_code, a_to.code to_code, p_model.name as p_model_name, "
	        				+ "departure_time, arrival_time, TIME(departure_time) as time_in_departure, "
	        				+ "TIME(arrival_time) as time_in_arrival, flight_code, ss.name season, cl.name class, cdt.description description, price from flights fl "
	        				+ "INNER JOIN prices pr ON fl.id = flight_id " 
	        				+ "INNER JOIN seasons ss ON season_id = ss.id "
	        				+ "INNER JOIN classes cl ON class_id = cl.id "
	        				+ "INNER JOIN conditions cdt ON pr.condition_id = cdt.id INNER JOIN routes r ON r.id = fl.route_id "
	        				+ "INNER JOIN planes p ON plane_id = p.id "
	        				+ "INNER JOIN plane_models p_model ON plane_model_id = p_model.id "
	        				+ "INNER JOIN airports a_fr ON a_fr.id = r.from_airport_id "
	        				+ "INNER JOIN airports a_to ON a_to.id = r.to_airport_id "  
	        				+ "ORDER BY price ASC;";
	        PreparedStatement statement;
	        List<Ticket> tickets = new ArrayList<Ticket>();

	        try {

	            statement = connection.prepareStatement(sqlQuery);
//	            statement.setDate(1, (Date) ticket.getDepartureDate());
	            // 	Executing statement
	            ResultSet results = statement.executeQuery();
	            
	            while(results.next()){

	                Ticket ticketElement = new Ticket();
	                Route route = new Route();
	                Airport fromAirport = new Airport();
	                Airport toAirport = new Airport();
	                
	                ticketElement.setPlaneName(results.getString("p_model_name"));
	                ticketElement.setDepartureDate(results.getDate("departure_time"));
	                ticketElement.setArrivalDate(results.getDate("arrival_time"));
	                ticketElement.setFlightCode(results.getString("flight_code"));
	                ticketElement.setConditionDes(results.getString("description"));
	                ticketElement.setClassName(results.getString("class"));
	                ticketElement.setSeasonName(results.getString("season"));
	                ticketElement.setPrice(results.getLong("price"));
	                ticketElement.setDepartureTime(results.getTime("time_in_departure"));
	                ticketElement.setArrivalTime(results.getTime("time_in_arrival"));
	                ticketElement.setRoute(route);
	                
	                fromAirport.setCode(results.getString("from_code"));
	                toAirport.setCode(results.getString("to_code"));
	                route.setAirportFrom(fromAirport);
	                route.setAirportTo(toAirport);
	                tickets.add(ticketElement);
	            }

	            results.close();
	            statement.close();


	        } catch (Exception e) {
	                // TODO Auto-generated catch block
	                e.printStackTrace();
	        } finally {
	                
	                return tickets;
	        }


	    }
	    
	 
}

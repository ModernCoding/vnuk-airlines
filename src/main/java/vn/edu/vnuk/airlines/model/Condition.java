package vn.edu.vnuk.airlines.model;

import javax.validation.constraints.NotNull;

public class Condition{
	private long id;
	private String name;
    private String description;
	
    @NotNull
	public long getId(){
		return id;
		}
	public String getName(){
		return name;
		}
	public String getDescription(){
		return description;
		}
		
	public void setId(long id){
		this.id = id;
		}
	public void setName(String name){
		this.name = name;
		}
	public void setDescription(String description){
		this.description = description;
		}
}
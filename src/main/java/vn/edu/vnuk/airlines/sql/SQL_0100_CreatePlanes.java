package vn.edu.vnuk.airlines.sql;

import java.sql.Connection;
import java.sql.SQLException;

public class SQL_0100_CreatePlanes {
	
	private final Connection connection;
	private final String sqlQuery;
	
	public SQL_0100_CreatePlanes(Connection connection) {
		super();
		this.connection = connection;
		this.sqlQuery = "create table IF NOT EXISTS planes ("
						+ "id BIGINT NOT NULL AUTO_INCREMENT, "
						+ "number_of_seat BIGINT, "
						+ "plane_model_id BIGINT, "
						+ "primary key (id), "
						+ "FOREIGN KEY (plane_model_id) REFERENCES plane_models(id)"
						+ ");";
	}
	
	public void run() throws SQLException {

			try {
			        connection.prepareStatement(sqlQuery).execute();
			        System.out.println("New table planes in DB !");
			
			} catch (Exception e) {
			        // TODO Auto-generated catch block
			        e.printStackTrace();
			} finally {
			        System.out.println("Done !");
			       // connection.close();
			}
			
		}
	
}
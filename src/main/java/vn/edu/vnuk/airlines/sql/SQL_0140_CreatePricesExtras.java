package vn.edu.vnuk.airlines.sql;

import java.sql.Connection;
import java.sql.SQLException;

public class SQL_0140_CreatePricesExtras {
	
	private final Connection connection;
	private final String sqlQuery;
	
	public SQL_0140_CreatePricesExtras(Connection connection) {
		super();
		this.connection = connection;
		this.sqlQuery = "create table IF NOT EXISTS prices_extras ("
						+ "id BIGINT NOT NULL AUTO_INCREMENT, "
						+ "price_id BIGINT NOT NULL, "
						+ "extra_id BIGINT NOT NULL, "
						+ "is_included_price BOOLEAN, "
						+ "primary key (id), "
						+ "foreign key (price_id) "
						+ "REFERENCES prices(id), "
						+ "foreign key (extra_id) "
						+ "REFERENCES extras(id)"
						+ ");";
	}
	
	public void run() throws SQLException {

			try {
			        connection.prepareStatement(sqlQuery).execute();
			        System.out.println("New table prices_extras in DB !");
			
			} catch (Exception e) {
			        // TODO Auto-generated catch block
			        e.printStackTrace();
			} finally {
			        System.out.println("Done !");
			        //connection.close();
			}
			
		}
	
}
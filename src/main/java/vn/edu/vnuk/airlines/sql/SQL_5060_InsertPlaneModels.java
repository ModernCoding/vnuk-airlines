package vn.edu.vnuk.airlines.sql;

import java.sql.Connection;
import java.sql.SQLException;



public class SQL_5060_InsertPlaneModels {
	private final Connection connection;
	private final String sqlQuery;
	
	public SQL_5060_InsertPlaneModels(Connection connection) {
		super();
		this.connection = connection;
		this.sqlQuery = "insert into plane_models (name, plane_manufacture_id) "
				+ "values ('Airbus A320',2), ('Airbus A330', 2), ('Airbus A350', 2), ('Airbus A380', 2),"
				+ " ('Boeing 737', 1), ('Boeing 777', 1), ('Boeing 787', 1)  ";
	}
	
	public void run() throws SQLException {

			try {
			        connection.prepareStatement(sqlQuery).execute();
			        System.out.println("New data in table plane_models in DB !");
			
			} catch (Exception e) {
			        // TODO Auto-generated catch block
			        e.printStackTrace();
			} finally {
			        System.out.println("Done !");
			        //connection.close();
			}
			
	}
	
	
}

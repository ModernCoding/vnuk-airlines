package vn.edu.vnuk.airlines.controller;

import java.sql.SQLException;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import vn.edu.vnuk.airlines.dao.SeasonDao;
import vn.edu.vnuk.airlines.model.Season;

@Controller
public class SeasonsController {

	private final SeasonDao dao;
	
	@Autowired
	public SeasonsController(SeasonDao dao) {
		this.dao = dao;
	}
		
	 @RequestMapping("season/new")
	    public String add(){
	        return "season/new";
	 }
	
	 @RequestMapping("season/create")
	    public String create(@Valid Season season, BindingResult result) throws SQLException{
	        dao.create(season);
	        return "redirect:/seasons";
	 }
	 
	 @RequestMapping("seasons")
	    public String read(Model model) throws SQLException{
	        model.addAttribute("seasons", dao.read());
	        return "season/index";
	 }
	 @RequestMapping("season/show")
	    public String show(@RequestParam Map<String, String> seasonId, Model model) throws SQLException{
	        int id = Integer.parseInt(seasonId.get("id").toString());
	        model.addAttribute("season", dao.read(id));
	        return "season/show";
	    }
	 @RequestMapping("season/edit")
	    public String edit(@RequestParam Map<String, String> seasonId, Model model) throws SQLException{
	        int id = Integer.parseInt(seasonId.get("id").toString());
	        model.addAttribute("season", dao.read(id));
	        return "season/edit";
	    }
	 
	 @RequestMapping("season/update")
	    public String update(@Valid Season season, BindingResult result) throws SQLException{
		 	

	        dao.update(season);
	        return "redirect:/seasons";
	    }
	//  DELETE WITH AJAX
	    @RequestMapping(value="season/delete", method = RequestMethod.POST)
	    public void delete(int id, HttpServletResponse response) throws SQLException {
	        dao.delete(id);
	        response.setStatus(200);
	    }

	    
}

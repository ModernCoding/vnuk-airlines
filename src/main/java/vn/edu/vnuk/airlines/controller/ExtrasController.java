package vn.edu.vnuk.airlines.controller;

import java.sql.SQLException;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import vn.edu.vnuk.airlines.dao.ExtraDao;
import vn.edu.vnuk.airlines.model.Extra;



@Controller
public class ExtrasController {

	private final ExtraDao dao;
	
	@Autowired
	public ExtrasController(ExtraDao dao) {
		this.dao = dao;
	}
	
	@RequestMapping("extra/new")
    public String add(){
        return "extra/new";
	}
	
	@RequestMapping("extra/create")
    public String create(@Valid Extra extra, BindingResult result) throws SQLException{
        dao.create(extra);
        return "redirect:/extras";
	}
	
	@RequestMapping("extras")
    public String read(Model model) throws SQLException{
        model.addAttribute("extras", dao.read());
        return "extra/index";
	}
	
	@RequestMapping("extra/show")
    public String show(@RequestParam Map<String, String> extraId, Model model) throws SQLException{
        int id = Integer.parseInt(extraId.get("id").toString());
        model.addAttribute("extra", dao.read(id));
        return "extra/show";
    }
	
	@RequestMapping("extra/edit")
    public String edit(@RequestParam Map<String, String> extraId, Model model) throws SQLException{
        int id = Integer.parseInt(extraId.get("id").toString());
        model.addAttribute("extra", dao.read(id));
        return "extra/edit";
    }
	
	@RequestMapping("extra/update")
    public String update(@Valid Extra extra, BindingResult result) throws SQLException{
	 	
        dao.update(extra);
        return "redirect:/extras";
    }
	
//  DELETE WITH AJAX
    @RequestMapping(value="extra/delete", method = RequestMethod.POST)
    public void delete(int id, HttpServletResponse response) throws SQLException {
        dao.delete(id);
        response.setStatus(200);
    }
	
}

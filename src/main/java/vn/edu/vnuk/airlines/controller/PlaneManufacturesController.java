package vn.edu.vnuk.airlines.controller;

import java.sql.SQLException;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import vn.edu.vnuk.airlines.dao.PlaneManufactureDao;
import vn.edu.vnuk.airlines.model.PlaneManufacture;

@Controller
public class PlaneManufacturesController {

	private final PlaneManufactureDao dao;
	
	@Autowired
	public PlaneManufacturesController(PlaneManufactureDao dao) {
		this.dao = dao;
	}
		
	 @RequestMapping("planeManufacture/new")
	    public String add(){
	        return "planeManufacture/new";
	 }
	
	 @RequestMapping("planeManufacture/create")
	    public String create(@Valid PlaneManufacture planeManufacture, BindingResult result) throws SQLException{
	        dao.create(planeManufacture);
	        return "redirect:/planeManufactures";
	 }
	 
	 @RequestMapping("planeManufactures")
	    public String read(Model model) throws SQLException{
	        model.addAttribute("planeManufactures", dao.read());
	        return "planeManufacture/index";
	 }
	 @RequestMapping("planeManufacture/show")
	    public String show(@RequestParam Map<String, String> planeManufactureId, Model model) throws SQLException{
	        int id = Integer.parseInt(planeManufactureId.get("id").toString());
	        model.addAttribute("planeManufacture", dao.read(id));
	        return "planeManufacture/show";
	    }
	 
	 @RequestMapping("planeManufacture/edit")
	    public String edit(@RequestParam Map<String, String> planeManufactureId, Model model) throws SQLException{
	        int id = Integer.parseInt(planeManufactureId.get("id").toString());
	        model.addAttribute("planeManufacture", dao.read(id));
	        return "planeManufacture/edit";
	    }
	 
	 @RequestMapping("planeManufacture/update")
	    public String update(@Valid PlaneManufacture planeManufacture, BindingResult result) throws SQLException{
		 	

	        dao.update(planeManufacture);
	        return "redirect:/planeManufactures";
	    }
	//  DELETE WITH AJAX
	    @RequestMapping(value="planeManufacture/delete", method = RequestMethod.POST)
	    public void delete(int id, HttpServletResponse response) throws SQLException {
	        dao.delete(id);
	        response.setStatus(200);
	    }
}

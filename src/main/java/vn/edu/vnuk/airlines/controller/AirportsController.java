package vn.edu.vnuk.airlines.controller;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import vn.edu.vnuk.airlines.dao.AirportDao;
import vn.edu.vnuk.airlines.dao.CityDao;
import vn.edu.vnuk.airlines.model.Airport;
import vn.edu.vnuk.airlines.model.City;

@Controller
public class AirportsController {

	private final AirportDao airportDao;
	private final CityDao cityDao;
	
	@Autowired
	public AirportsController(AirportDao airportDao, CityDao cityDao) {
		this.cityDao = cityDao;
		this.airportDao = airportDao;
	}
		
	 @RequestMapping("airport/new")
	    public String add(Model model){
		 	model.addAttribute("airport" ,new Airport());
	        return "airport/new";
	 	}
	 	
	
	 @RequestMapping("airport/create")
	    public String create(@Valid Airport airport, BindingResult result) throws SQLException{
		 	airportDao.create(airport);
	        return "redirect:/airports";
	 }
	 
	 @RequestMapping("airports")
	    public String read(Model model) throws SQLException{
	        model.addAttribute("airports", airportDao.read());
	        return "airport/index";
	 }
	 
	 @RequestMapping("airport/show")
	    public String show(@RequestParam Map<String, String> airportId, Model model) throws SQLException{
	        int id = Integer.parseInt(airportId.get("id").toString());
	        model.addAttribute("airport", airportDao.read(id));
	        return "airport/show";
	    }
	 
	 @RequestMapping("airport/edit")
	    public String edit(@RequestParam Map<String, String> airportId, Model model) throws SQLException{
	        int id = Integer.parseInt(airportId.get("id").toString());
	        model.addAttribute("airport", airportDao.read(id));
	        return "airport/edit";
	    }
	 
	 @RequestMapping("airport/update")
	    public String update(@Valid Airport airport, BindingResult result) throws SQLException{

	        airportDao.update(airport);
	        return "redirect:/airports";
	    }
	//  DELETE WITH AJAX
	    @RequestMapping(value="airport/delete", method = RequestMethod.POST)
	    public void delete(int id, HttpServletResponse response) throws SQLException {
	        airportDao.delete(id);
	        response.setStatus(200);
	    }
	    
	    @ModelAttribute("cityList")
	    public Map<Long, String> getCountries(){
	    	Map<Long, String> cityList = new HashMap<Long, String>();
	    	try {
	    		for(City city : cityDao.read()) {
	    			cityList.put(city.getId(), city.getName());
	    		}
	    	}  catch (SQLException e) {
	    		 // TODO Auto-generated catch block
	    		e.printStackTrace();
	    	}
	    	return cityList;
	    }
}

package vn.edu.vnuk.airlines.controller;

import java.sql.SQLException;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import vn.edu.vnuk.airlines.dao.ClassDao;
import vn.edu.vnuk.airlines.model.Classe;

@Controller
public class ClassesController {

	private final ClassDao dao;
	
	@Autowired
	public ClassesController(ClassDao dao) {
		this.dao = dao;
	}
		
	 @RequestMapping("classe/new")
	    public String add(){
	        return "classe/new";
	 }
	
	 @RequestMapping("classe/create")
	    public String create(@Valid Classe classe, BindingResult result) throws SQLException{
	        dao.create(classe);
	        return "redirect:/classes";
	 }
	 
	 @RequestMapping("classes")
	    public String read(Model model) throws SQLException{
	        model.addAttribute("classes", dao.read());
	        return "classe/index";
	 }

	 @RequestMapping("classe/show")
	    public String show(@RequestParam Map<String, String> classId, Model model) throws SQLException{
	        int id = Integer.parseInt(classId.get("id").toString());
	        model.addAttribute("classe", dao.read(id));
	        return "classe/show";
	    }
	 
	 @RequestMapping("classe/edit")
	    public String edit(@RequestParam Map<String, String> classId, Model model) throws SQLException{
	        int id = Integer.parseInt(classId.get("id").toString());
	        model.addAttribute("classe", dao.read(id));
	        return "classe/edit";
	    }
	 
	 @RequestMapping("classe/update")
	    public String update(@Valid Classe classe, BindingResult result) throws SQLException{
		 	

	        dao.update(classe);
	        return "redirect:/classes";
	    }
	//  DELETE WITH AJAX
	    @RequestMapping(value="classe/delete", method = RequestMethod.POST)
	    public void delete(int id, HttpServletResponse response) throws SQLException {
	        dao.delete(id);
	        response.setStatus(200);
	    }

}

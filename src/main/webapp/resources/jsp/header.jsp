<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<div id="page"> 
<div class="page-inner">  
	<nav class="gtco-nav" role="navigation">
		<div class="gtco-container">
			<div class="row">
				<div class="col-sm-4 col-xs-12">
					<div id="gtco-logo"><a href="/vnuk">TAT AirLine <em>.</em></a></div>
				</div>
				<div class="col-xs-8 text-right menu-1">
					<ul>
						<li class="has-dropdown">
							<a href="#">Airport</a>
							<ul class="dropdown">
								<li><a href="/vnuk/countries">Country</a></li>
								<li><a href="/vnuk/cities">City</a></li>
								<li><a href="/vnuk/airports">Airport</a></li>
								<li><a href="/vnuk/routes">Route</a></li>
							</ul>
						</li>
						<li class="has-dropdown">
							<a href="#">Plane</a>
							<ul class="dropdown">
								<li><a href="/vnuk/planeManufactures">Manufacture</a></li>
								<li><a href="/vnuk/planeModels">Model</a></li>
								<li><a href="/vnuk/planes">Plane</a></li>
							</ul>
						</li>
						<li class="has-dropdown">
							<a href="#">Flight</a>
							<ul class="dropdown">
								<li><a href="/vnuk/days">Day</a></li>
								<li><a href="/vnuk/flights">Flight</a></li>
								<li><a href="/vnuk/bookTicket">Ticket</a></li>
							</ul>
						</li>
						<li class="has-dropdown">
							<a href="#">Price</a>
							<ul class="dropdown">
								<li><a href="/vnuk/seasons">Season</a></li>
								<li><a href="/vnuk/conditions">Condition</a></li>
								<li><a href="/vnuk/classes">Class</a></li>
								<li><a href="/vnuk/extras">Extra</a></li>
								<li><a href="/vnuk/priceExtras">Price Extra</a></li>
								<li><a href="/vnuk/prices">Price</a></li>
							</ul>
						</li>
					</ul>	
				</div>
			</div>
		</div>
	</nav>
	</div>
	</div>
	

	


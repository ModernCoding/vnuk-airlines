<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<c:import url="/resources/jsp/header_booking.jsp" />
    
    <div id="my-notice"></div>
    <table class="table table-bordered table-hover table-responsive table-striped">
        <thead>
            <tr>
                <th>Route</th>
                <th>Plane</th>
                <th>Departure Date</th>
                <th>Arrival Date</th>
                <th>Flight Code</th>
                <th>Season</th>
                <th>Class</th>
                <th>Condition</th>
                <th>Price</th>
            </tr>
        </thead>

        <tbody>

            <c:forEach var="ticket" items="${tickets}">

                <tr>
                    <td>${ticket.route.airportFrom.code} - ${ticket.route.airportTo.code}</td>
                    <td>${ticket.planeName}</td>
                    <td>${ticket.departureDate} - ${ticket.departureTime}</td>
                    <td>${ticket.arrivalDate} - ${ticket.arrivalTime}</td>
                    <td>${ticket.flightCode}</td>
                    <td>${ticket.seasonName}</td>
                    <td>${ticket.className}</td>
                    <td>${ticket.conditionDes}</td>
                    <td>${ticket.price}</td>
               </tr>

            </c:forEach>

        </tbody>
    </table>
<c:import url="/resources/jsp/body-close-airports.jsp" />

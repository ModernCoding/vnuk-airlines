<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<c:import url="/resources/jsp/header_booking.jsp" />

    <a href="priceExtra/new" class="btn btn-primary">
        <i class="fa fa-plus" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;New Price Extra
    </a>

    <br /><br />
    
    <div id="my-notice"></div>
    
    <table class="table table-bordered table-hover table-responsive table-striped">
        <thead>
            <tr>
                <th></th>
                <th>Id</th>
                <th>Price</th>
                <th>Extra</th>
                <th>Is Included Price</th>
            </tr>
        </thead>

        <tbody>

            <c:forEach var="priceExtra" items="${priceExtras}">

                <tr>
                    <td>
                    	<a href="priceExtra/show?id=${priceExtra.id}" class="btn btn-xs btn-success" 
                           data-toggle="tooltip" title="Show priceExtra" data-placement="bottom">
                            <i class="fa fa-eye" aria-hidden="true"></i>
                        </a>
                    
                        <a href="priceExtra/edit?id=${priceExtra.id}" class="btn btn-xs btn-primary" 
                           data-toggle="tooltip" title="Edit priceExtra" data-placement="bottom">
                            <i class="fa fa-pencil" aria-hidden="true"></i>
                        </a>
                       
                       <%--    DELETING WITH AJAX   --%>

                        <button type="button"
                                class="btn btn-xs btn-danger my-priceExtra-to-delete" 
                                value="${priceExtra.id}"
                                data-toggle="tooltip" 
                                title="Delete route" 
                                data-placement="bottom">
                            <i class="fa fa-trash" aria-hidden="true"></i>
                        </button>

                    </td>
                    
                    <td>${priceExtra.id}</td>
                    <td>${priceExtra.prices.price}</td>
                    <td>${priceExtra.extra.name}</td>
                    <td>${priceExtra.isIncludedPrice}</td>
                    
               </tr>

            </c:forEach>

        </tbody>
    </table>
            
<c:import url="/resources/jsp/body-close-priceExtras.jsp" />

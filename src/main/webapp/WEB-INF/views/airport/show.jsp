<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>


<c:import url="/resources/jsp/header_booking.jsp" />
    <h3>Show detail airport ${airport.id}</h3>

    <form action="../airport/show" method="post">
    	<table class="table table-bordered table-hover table-responsive table-striped">
	        <thead>
	            <tr>
	                <th>Id</th>
	                <th>Code</th>
	                <th>City</th>
	            </tr>
	        </thead>

	        <tbody>
			       <tr>
			       		<td>${airport.id}</td>
			       		<td>${airport.code}</td>
						<td>${airport.city.name}</td>
			       </tr>
	        </tbody>
		 </table>
		 
        <a class="btn btn-default" href="../airports">
            <i class="fa fa-times-rectangle-o" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Back
        </a>
    </form>
<c:import url="/resources/jsp/body-close.jsp" /> 
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>


<c:import url="/resources/jsp/header_booking.jsp" />
    <h3>Show detail plane  n� ${plane.id}</h3>v

    <form action="../plane/update" method="post">
    	<table class="table table-bordered table-hover table-responsive table-striped">
	        <thead>
	            <tr>
	                <th>Id</th>
	                <th>Number Of Seat</th>
	                <th>Plane Model</th>
	            </tr>
	        </thead>
	
	        <tbody>
	        	<tr>
	        		<td>${plane.id}</td>
	        		<td>${plane.numberOfSeat}</td>
	        		<td>${plane.planeModel.name}</td>
	        	</tr>
	        </tbody>
	    </table>    
		
        <a class="btn btn-default" href="../planes">
            <i class="fa fa-times-rectangle-o" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Cancel
        </a>
    </form>
<c:import url="/resources/jsp/body-close.jsp" />

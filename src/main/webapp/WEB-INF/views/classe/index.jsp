<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<c:import url="/resources/jsp/header_booking.jsp" />

    <a href="classe/new" class="btn btn-primary">
        <i class="fa fa-plus" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;New Class
    </a>

    <br /><br />
    
    <div id="my-notice"></div>
    
    <table class="table table-bordered table-hover table-responsive table-striped">
        <thead>
            <tr>
                <th></th>
                <th>Id</th>
                <th>Name</th>
            </tr>
        </thead>

        <tbody>

            <c:forEach var="classe" items="${classes}">

                <tr>
                    <td>
                     	<a href="classe/show?id=${classe.id}" class="btn btn-xs btn-success" 
                           data-toggle="tooltip" title="Show class" data-placement="bottom">
                            <i class="fa fa-eye" aria-hidden="true"></i>
                        </a>
                        
                        <a href="classe/edit?id=${classe.id}" class="btn btn-xs btn-primary" 
                           data-toggle="tooltip" title="Edit class" data-placement="bottom">
                            <i class="fa fa-pencil" aria-hidden="true"></i>
                        </a>
                       
                       <%--    DELETING WITH AJAX   --%>

                        <button type="button"
                                class="btn btn-xs btn-danger my-classe-to-delete" 
                                value="${classe.id}"
                                data-toggle="tooltip" 
                                title="Delete class" 
                                data-placement="bottom">
                            <i class="fa fa-trash" aria-hidden="true"></i>
                        </button>

                    </td>
                    
                    <td>${classe.id}</td>
                    <td>${classe.name}</td>
                    
               </tr>

            </c:forEach>

        </tbody>
    </table>
            
<c:import url="/resources/jsp/body-close-classes.jsp" />

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<c:import url="/resources/jsp/header_booking.jsp" />

    <a href="condition/new" class="btn btn-primary">
        <i class="fa fa-plus" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;New Condition
    </a>

    <br /><br />
    
    <div id="my-notice"></div>
    
    <table class="table table-bordered table-hover table-responsive table-striped">
        <thead>
            <tr>
                <th></th>
                <th>Id</th>
                <th>Name</th>
                <th>Description</th>
            </tr>
        </thead>

        <tbody>

            <c:forEach var="condition" items="${conditions}">

                <tr>
                    <td>
                    	<a href="condition/show?id=${condition.id}" class="btn btn-xs btn-success" 
                           data-toggle="tooltip" title="Show condition" data-placement="bottom">
                            <i class="fa fa-eye" aria-hidden="true"></i>
                        </a>
                    
                        <a href="condition/edit?id=${condition.id}" class="btn btn-xs btn-primary" 
                           data-toggle="tooltip" title="Edit condition" data-placement="bottom">
                            <i class="fa fa-pencil" aria-hidden="true"></i>
                        </a>
                       
                       <%--    DELETING WITH AJAX   --%>

                        <button type="button"
                                class="btn btn-xs btn-danger my-condition-to-delete" 
                                value="${condition.id}"
                                data-toggle="tooltip" 
                                title="Delete condition" 
                                data-placement="bottom">
                            <i class="fa fa-trash" aria-hidden="true"></i>
                        </button>

                    </td>
                    
                    <td>${condition.id}</td>
                    <td>${condition.name}</td>
                    <td>${condition.description}</td>
                    
               </tr>

            </c:forEach>

        </tbody>
    </table>
            
<c:import url="/resources/jsp/body-close-conditions.jsp" />

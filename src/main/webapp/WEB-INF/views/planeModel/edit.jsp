<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>


<c:import url="/resources/jsp/header_booking.jsp" />
    <h3>Update plane Model n� ${planeModel.id}</h3>

    <form action="../planeModel/update" method="post">
        <input type="hidden" name="id" value="${planeModel.id}" />
        Name : <input type="text" name="name" value="${planeModel.name}"/>
        Plane Manufacture Id: <br/>
        <form:select path = "planeModel.planeManufactureId">
        	<form:options items = "${planeManufactureList}" />
        </form:select>nufactureId}"/>
       
		<button type="submit" class="btn btn-success">
            <i class="fa fa-repeat" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Update
        </button>
        <a class="btn btn-default" href="../planeModels">
            <i class="fa fa-times-rectangle-o" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Cancel
        </a>
    </form>
<c:import url="/resources/jsp/body-close.jsp" />

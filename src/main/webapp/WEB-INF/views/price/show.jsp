<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>


<c:import url="/resources/jsp/header_booking.jsp" />
    <h3>Show detail price ${price.id}</h3>

    <form action="../price/update" method="post">
    	<table class="table table-bordered table-hover table-responsive table-striped">
	        <thead>
	            <tr>
	                <th>Id</th>
	                <th>Flight</th>
	                <th>Class</th>
	                <th>Season</th>
	                <th>Condition</th>
	                <th>Price</th>
	            </tr>
	        </thead>
	
	        <tbody>
	        	<tr>
	        		<td>${price.id}</td>
	        		<td>${price.flight.flightCode}</td>
	        		<td>${price.classe.name}</td>
	        		<td>${price.season.name}</td>
	        		<td>${price.condition.name}</td>
	        		<td>${price.price}</td>
	        	</tr>
	        </tbody>
	    </table>

        <a class="btn btn-default" href="../prices">
            <i class="fa fa-times-rectangle-o" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Cancel
        </a>
    </form>
<c:import url="/resources/jsp/body-close.jsp" />

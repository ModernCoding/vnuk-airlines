<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<c:import url="/resources/jsp/header_booking.jsp" />

    <a href="price/new" class="btn btn-primary">
        <i class="fa fa-plus" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;New Price
    </a>

    <br /><br />
    
    <div id="my-notice"></div>
    
    <table class="table table-bordered table-hover table-responsive table-striped">
        <thead>
            <tr>
                <th></th>
                <th>Id</th>
                <th>Flight</th>
                <th>Class</th>
                <th>Season</th>
                <th>Condition</th>
                <th>Price</th>
            </tr>
        </thead>

        <tbody>

            <c:forEach var="price" items="${prices}">

                <tr>
                    <td>
                    	<a href="price/show?id=${price.id}" class="btn btn-xs btn-success" 
                           data-toggle="tooltip" title="Show price" data-placement="bottom">
                            <i class="fa fa-eye" aria-hidden="true"></i>
                        </a>
                    
                        <a href="price/edit?id=${price.id}" class="btn btn-xs btn-primary" 
                           data-toggle="tooltip" title="Edit price" data-placement="bottom">
                            <i class="fa fa-pencil" aria-hidden="true"></i>
                        </a>
                       
                       <%--    DELETING WITH AJAX   --%>

                        <button type="button"
                                class="btn btn-xs btn-danger my-price-to-delete" 
                                value="${price.id}"
                                data-toggle="tooltip" 
                                title="Delete price" 
                                data-placement="bottom">
                            <i class="fa fa-trash" aria-hidden="true"></i>
                        </button>

                    </td>
                    
                    <td>${price.id}</td>
                    <td>${price.flight.flightCode}</td>
                    <td>${price.classe.name}</td>
                    <td>${price.season.name}</td>
                    <td>${price.condition.description}</td>
                    <td>${price.price}</td>
                    
               </tr>

            </c:forEach>

        </tbody>
    </table>
            
<c:import url="/resources/jsp/body-close-prices.jsp" />

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<c:import url="/resources/jsp/header_booking.jsp" />

    <a href="extra/new" class="btn btn-primary">
        <i class="fa fa-plus" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;New Extra
    </a>

    <br/><br />
    
    <div id="my-notice"></div>
    
    <table class="table table-bordered table-hover table-responsive table-striped">
        <thead>
            <tr>
                <th></th>
                <th>Id</th>
                <th>Name</th>
            </tr>
        </thead>

        <tbody>

            <c:forEach var="extra" items="${extras}">

                <tr>
                    <td>
                    	<a href="extra/show?id=${extra.id}" class="btn btn-xs btn-success" 
                           data-toggle="tooltip" title="Show extra" data-placement="bottom">
                            <i class="fa fa-eye" aria-hidden="true"></i>
                        </a>
                    
                        <a href="extra/edit?id=${extra.id}" class="btn btn-xs btn-primary" 
                           data-toggle="tooltip" title="Edit extra" data-placement="bottom">
                            <i class="fa fa-pencil" aria-hidden="true"></i>
                        </a>
                       
                       <%--    DELETING WITH AJAX   --%>

                        <button type="button"
                                class="btn btn-xs btn-danger my-extra-to-delete" 
                                value="${extra.id}"
                                data-toggle="tooltip" 
                                title="Delete extra" 
                                data-placement="bottom">
                            <i class="fa fa-trash" aria-hidden="true"></i>
                        </button>

                    </td>
                    
                    <td>${extra.id}</td>
                    <td>${extra.name}</td>
                    
               </tr>

            </c:forEach>

        </tbody>
    </table>
            
<c:import url="/resources/jsp/body-close-extras.jsp" />
